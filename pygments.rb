#!/usr/bin/env ruby

class Pygments
  class << self
    attr_accessor :bin

    def bin
      @bin ||= `which pygmentize`.chomp
    end

    def set opts = {}
      return if opts.nil? or opts.length == 0
      '-O ' + opts.map {|k,v| "#{k}=#{v}"}.join(',')
    end

    def lexer file
      `#{bin} -N #{file}`.chomp rescue 'text'
    end
  end

  def initialize
    Pygments.bin
    @opts = {}
  end

  def set opts = {}
    @opts = (opts.dup rescue {})
  end

  def pygmentize text = '', lexer = 'text'
    opts = Pygments.set @opts
    open "| #{Pygments.bin} -f html #{opts} -l #{lexer}", "r+" do |io|
      io.write(text)
      io.close_write
      io.read
    end
  end
end

class String
  def pygmentize lexer = 'text', options = {}
    pyg = Pygments.new
    pyg.set options
    pyg.pygmentize self, lexer
  end
end

