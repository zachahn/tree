#!/usr/bin/env ruby
require 'sinatra/base'
%w(grit haml rdiscount redcloth pygments).each { |l| require l }

class Tree < Sinatra::Base
  set :project => '.'
  set :default => 'README'

  set :app_file => __FILE__
  set :inline_templates => true

  class Project
    def initialize path
      @repo = Grit::Repo.new path
    end

    def /(path = nil)
      return if path.nil? or path.empty? or path == '/'
      @repo.tree/path
    end

    def ls_tree treeish = 'HEAD'
      ls_tree = Proc.new { |t| @repo.git.ls_tree({}, t).split "\n" }
      indent  = Proc.new { |s| s.split("\n").map { |l| "  #{l}\n"}.join }

      recurse = Proc.new do |tree|
        out = ""
        ls_tree.call(tree).map { |i| i[7..-1].split }.each do |i|
          treeish, sub = tree.split ':'
          if    i[0] == 'blob'
            out << "%li <a href='#{sub && "/#{sub}"}/#{i[2]}'>#{i[2]}</a>\n"
          elsif i[0] == 'tree'
            out << "%li\n"
            out << "  #{i[2]}\n"
            out << "  %ul\n"
            path = "#{treeish}:#{sub && "#{sub}/"}#{i[2]}"
            out << indent.call(indent.call(recurse.call(path)))
          end
        end
        out
      end

      "%ul\n" + indent.call(recurse.call(treeish))
    end
  end

  before do
    @project = Project.new options.project
    @title   = File.expand_path(options.project).split('/')[-1]
    Pygments.bin = options.pygpath rescue nil
  end

  helpers do
    def title title = nil
      @title = "#{@title}/#{title}" if title
      @title
    end
  end

  get '/' do
    redirect "/#{options.default}" rescue raise Sinatra::NotFound
  end

  get '/*' do
    pass if params['splat'][0] =~ /^~\/.*$/
    blob = @project/(path = params['splat'][0].chomp('/'))
    raise Sinatra::NotFound unless blob.class == Grit::Blob

    title path unless path.empty?

    case File.extname blob.name
    when '.md', '.markdown'
      @content = RDiscount.new(blob.data).to_html
    when '.t', '.textile'
      @content = RedCloth.new(blob.data).to_html
    else
      lexer = Pygments.lexer blob.name
      opt = { :linenos => 1 } unless blob.name == options.default rescue nil
      @content = blob.data.pygmentize lexer, opt
    end
    haml :content
  end

  get '/~/*' do
    blob = @project/(path = params['splat'][0].chomp('/'))
    raise Exception unless blob.class == Grit::Blob
    raise Exception unless 8*1024

    content_type blob.mime_type
    blob.data
  end

  not_found do
    @content = "<h1>Use the navigation tree on the left to view the " +
               "files in this repository</h1>"
    haml :content
  end
end

__END__
@@ content
!!! 5
%html
  %head
    - %w[reset style code].each do |css|
      %link{:href=>"/~#{css}.css", :rel=>'stylesheet', :type=>'text/css'}
    %title= title
  %body
    #wrap
      #tree
        = haml @project.ls_tree
      #content
        ~ @content

